#!/usr/bin/env python3
"Jason Hart Python Cert project"
## import paramiko and getpass
import getpass
import paramiko
import warnings

# filter Warnings
warnings.filterwarnings(action="ignore", module=".*paramiko.*")

def main():
  """run-time code"""

  ## where to connect to
  print("Server to SSH to.")
  server = input()
  print("User name to use for SSH")
  user = input()

  s = paramiko.SSHClient()
  ## Set auto accpet key policy
  s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  mykey = paramiko.RSAKey.from_private_key_file(r"C:\Users\jason_hart\.ssh\id_rsa")

  ## Get password and connect
  print("Password for SSH connection:")
  s.connect(server, username=user, password=getpass.getpass(), pkey=mykey, timeout=11)


  stdin, stdout, stderr = s.exec_command("ls -l")
  print (stdout.read().decode(utf-8))
  #print(sessout.read().decode('utf-8'))

  ## close the connections
  s.close()  # close ssh connection


if __name__ == "__main__":
  main()
